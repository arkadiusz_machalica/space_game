class AddColumnTierToShipsAndRenameKlassColumns < ActiveRecord::Migration
  def change
    rename_column :ship_stats, :klass, :variant
    add_column :ship_stats, :tier, :integer, default: 1, null: false
    rename_column :ships, :klass, :variant
    add_column :ships, :tier, :integer, default: 1, null: false
    add_column :engines, :tier, :integer, default: 1, null: false
    add_column :engines, :variant, :integer, default: 1, null: false
  end
end
