class AddsPartsAndWorkersToShips < ActiveRecord::Migration
  def change
    add_column :workers, :ship_id, :integer
    add_column :engines, :ship_id, :integer
    
    add_index :workers, :ship_id
    add_index :engines, :ship_id
    add_index :ships, :owner_id
    add_index :ships, :stat_id
  end
end
