class AddClusterNumToClusters < ActiveRecord::Migration
  def change
    add_column :clusters, :bridge_slots, :integer, default: 0
    add_column :clusters, :section_num, :integer
  end
end
