class CreateEngines < ActiveRecord::Migration
  def change
    create_table :engines do |t|
      t.string :kind
      t.integer :owner_id
      t.integer :cluster_id
      t.integer :stat_id
      t.integer :power, default: 0
      t.integer :hp
    end
    add_index :engines, :owner_id
    add_index :engines, :cluster_id
    add_index :engines, :stat_id
  end
end
