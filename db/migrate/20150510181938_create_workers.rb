class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :name
      t.references :employer, index: true
      t.references :crewable, polymorphic: true, index: true
      t.integer :engine, default: 0
      t.integer :kineticgun, default: 0
      t.integer :plating, default: 0
      t.integer :bay, default: 0
    end
  end
end
