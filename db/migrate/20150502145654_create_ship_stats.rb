class CreateShipStats < ActiveRecord::Migration
  def change
    create_table :ship_stats do |t|
      t.string :kind, unique: true
      t.text :stats
    end
    add_index :ship_stats, :kind, unique: true
  end
end
