class CreatePartStats < ActiveRecord::Migration
  def change
    create_table :part_stats do |t|
      t.string :section
      t.string :kind
      t.string :stats
    end
    add_index :part_stats, [:section, :kind], unique: true
  end
end
