class RenameKineticColumnInShips < ActiveRecord::Migration
  def change
    rename_column :ships, :kinetic, :kineticgun
  end
end
