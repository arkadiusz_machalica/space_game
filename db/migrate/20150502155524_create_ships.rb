class CreateShips < ActiveRecord::Migration
  def change
    create_table :ships do |t|
      t.string :name
      t.string :kind
      t.integer :owner_id
      t.integer :stat_id
      t.integer :plating, default: 0
      t.integer :kinetic, default: 0
      t.integer :engine, default: 0

      t.timestamps
    end
    add_index :ships, :name, unique: true
  end
end
