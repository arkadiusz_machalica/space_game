class CreateClusters < ActiveRecord::Migration
  def change
    create_table :clusters do |t|
      t.string :section
      t.integer :ship_id
      t.integer :engine_slots, default: 0
      t.integer :bay_slots, default: 0
      t.integer :plating_slots, default: 0
      t.integer :kineticgun_slots, default: 0
    end
    add_index :clusters, :ship_id
  end
end
