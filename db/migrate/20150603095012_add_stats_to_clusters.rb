class AddStatsToClusters < ActiveRecord::Migration
  def change
    add_column :clusters, :stats, :string
  end
end
