class AddColumnKlassToShipsAndShipStats < ActiveRecord::Migration
  def change
    add_column :ships, :klass, :integer, default: 1, null: false
    add_column :ship_stats, :klass, :integer, default: 1, null: false
  end
end
