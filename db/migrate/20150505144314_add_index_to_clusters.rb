class AddIndexToClusters < ActiveRecord::Migration
  def change
    add_index :clusters, [:ship_id, :section]
  end
end
