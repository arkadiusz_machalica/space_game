# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150611184753) do

  create_table "clusters", force: true do |t|
    t.string  "section"
    t.integer "ship_id"
    t.integer "engine_slots",     default: 0
    t.integer "bay_slots",        default: 0
    t.integer "plating_slots",    default: 0
    t.integer "kineticgun_slots", default: 0
    t.integer "bridge_slots",     default: 0
    t.integer "section_num"
    t.string  "stats"
  end

  add_index "clusters", ["ship_id", "section"], name: "index_clusters_on_ship_id_and_section"
  add_index "clusters", ["ship_id"], name: "index_clusters_on_ship_id"

  create_table "engines", force: true do |t|
    t.string  "kind"
    t.integer "owner_id"
    t.integer "cluster_id"
    t.integer "stat_id"
    t.integer "power",      default: 0
    t.integer "hp"
    t.integer "ship_id"
    t.integer "tier",       default: 1, null: false
    t.integer "variant",    default: 1, null: false
  end

  add_index "engines", ["cluster_id"], name: "index_engines_on_cluster_id"
  add_index "engines", ["owner_id"], name: "index_engines_on_owner_id"
  add_index "engines", ["ship_id"], name: "index_engines_on_ship_id"
  add_index "engines", ["stat_id"], name: "index_engines_on_stat_id"

  create_table "part_stats", force: true do |t|
    t.string "section"
    t.string "kind"
    t.string "stats"
  end

  add_index "part_stats", ["section", "kind"], name: "index_part_stats_on_section_and_kind", unique: true

  create_table "players", force: true do |t|
    t.string   "name"
    t.integer  "cash"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
  end

  add_index "players", ["email"], name: "index_players_on_email", unique: true
  add_index "players", ["name"], name: "index_players_on_name", unique: true
  add_index "players", ["reset_password_token"], name: "index_players_on_reset_password_token", unique: true

  create_table "ship_stats", force: true do |t|
    t.string  "kind"
    t.text    "stats"
    t.integer "variant", default: 1, null: false
    t.integer "tier",    default: 1, null: false
  end

  add_index "ship_stats", ["kind"], name: "index_ship_stats_on_kind", unique: true

  create_table "ships", force: true do |t|
    t.string   "name"
    t.string   "kind"
    t.integer  "owner_id"
    t.integer  "stat_id"
    t.integer  "plating",    default: 0
    t.integer  "kineticgun", default: 0
    t.integer  "engine",     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "variant",    default: 1, null: false
    t.integer  "tier",       default: 1, null: false
  end

  add_index "ships", ["name"], name: "index_ships_on_name", unique: true
  add_index "ships", ["owner_id"], name: "index_ships_on_owner_id"
  add_index "ships", ["stat_id"], name: "index_ships_on_stat_id"

  create_table "workers", force: true do |t|
    t.string  "name"
    t.integer "employer_id"
    t.integer "crewable_id"
    t.string  "crewable_type"
    t.integer "engine",        default: 0
    t.integer "kineticgun",    default: 0
    t.integer "plating",       default: 0
    t.integer "bay",           default: 0
    t.integer "ship_id"
  end

  add_index "workers", ["crewable_id", "crewable_type"], name: "index_workers_on_crewable_id_and_crewable_type"
  add_index "workers", ["employer_id"], name: "index_workers_on_employer_id"
  add_index "workers", ["ship_id"], name: "index_workers_on_ship_id"

end
