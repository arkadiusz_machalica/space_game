require 'yaml'

def msg(desciption, time)
  puts "-- #{desciption}\n   -> #{(Time.now - time).seconds.round(4)}s"
end

puts "\n---- SEEDS ----\n"
total_start = Time.now

# players creation
operation_start = Time.now
Player.destroy_all
players_id = []

player = Player.create(
  name: 'Beziq',
  email: 'beziq@example.com',
  password: 'asdfasdf',
  password_confirmation: 'asdfasdf'
  )
players_id << player.id
player = Player.create(
  name: 'Sammael',
  email: 'sammael@example.com',
  password: 'sammael_login',
  password_confirmation: 'sammael_login'
  )
players_id << player.id
player = Player.create(
  name: 'Gosc',
  email: 'gosc@example.com',
  password: 'qwerty1234',
  password_confirmation: 'qwerty1234'
  )
players_id << player.id

3.times do 
  nickname = Faker::Internet.user_name + Faker::Lorem.word
  player = Player.create(
    name:  nickname,
    email: "#{nickname}@example.com",
    password: 'asdfasdf',
    password_confirmation: 'asdfasdf'
    )
  players_id << player.id
end
msg('Players created', operation_start)

# ship stats creation
operation_start = Time.now
ShipStat.destroy_all
stats = YAML.load(File.read(
  File.expand_path('../../lib/assets/ship_types.yml', __FILE__)
  )).deep_symbolize_keys
stats[:ship].each do |code, stat|
  ShipStat.create(
    kind: code[2..-1],
    stats: stat.to_s
    )
end
msg('Ship stats created', operation_start)

# ships creation
operation_start = Time.now
Ship.destroy_all
5.times do |n|
  Ship.create(
    name: Ship.random_name,
    kind: "#{rand(1..10)}_#{rand(1..3)}",
    owner_id: players_id[n % 5],
    )
end
msg('Ships created', operation_start)

# part stats creation
operation_start = Time.now
PartStat.destroy_all
['engine', 'bay', 'kineticgun', 'emp', 'hack',
'plating', 'barrier', 'firewall', 'bridge'].each do |sect|
  stats = YAML.load(File.read(
    File.expand_path("../../lib/assets/#{sect}_types.yml", __FILE__)
    )).deep_symbolize_keys
  stats[sect.to_sym].each do |code, stat|
    PartStat.create(
      section: sect,
      kind: code[2..-1],
      stats: stat.to_s
      )
  end
end
msg('Part stats created', operation_start)

# Beziq & Sammael parts and workers creation
[
  Player.find_by(name: 'Beziq'),
  Player.find_by(name: 'Sammael'),
  Player.find_by(name: 'Gosc')
].each do |playr|
  operation_start = Time.now
  playr.get_paid(2_000_000)
  name = playr.name == 'Beziq' ? 'Rerum eos' : Ship.random_name
  ship = Ship.create(name: name, kind: '5_2', owner_id: playr.id)
  engines = []
  3.times do |n|
    engines << Engine.create(kind: "5_#{n+1}", owner_id: playr.id)
  end
  ship.install_parts(engines)
  workers = Worker.instantize(num: 14, topic: 'engine', range: 1)
  playr.buy(workers)
  engines[0].delegate_workers(workers: workers[0..3])
  engines[1].delegate_workers(workers: workers[4..7])
  engines[2].delegate_workers(workers: workers[8..14])
  msg("Player - #{playr.name} and elements created", operation_start)
end

puts "-" * 25
msg('Total seeding time', total_start)

