require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  def setup
    @tom = players(:tom)
    @bob = players(:bob)
  end
  
  test 'validations' do
    ['', 'a'*3, 'a'*26, @tom.name].each do |fail_name|
      player = Player.create(
        name: fail_name,
        email: 'player@example.com',
        password: 'asdfasdf',
        password_confirmation: 'asdfasdf'
        )
      assert_not player.valid?, "This name didn't fail, but should -- '#{player.name}'"
    end
  end
end
