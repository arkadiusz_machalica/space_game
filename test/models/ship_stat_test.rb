require 'test_helper'

class ShipStatTest < ActiveSupport::TestCase
  
  def setup
    @t_1_1 = ship_stats(:t_1_1)
    @t_5_2 = ship_stats(:t_5_2)
    @t_10_3 = ship_stats(:t_10_3)
  end
  
  test 'proper stats' do
    assert_equal 5000, @t_1_1.price
    assert_equal 115000, @t_5_2.price
    assert_equal 2500000, @t_10_3.price
  end
end
