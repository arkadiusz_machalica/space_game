require 'test_helper'

class ShipTest < ActiveSupport::TestCase
  def setup
    @s1_1 = Ship.create(kind: "1_1", name: "ship1")
    @s4_2 = Ship.create(kind: "4_2", name: "ship2")
    @s7_3 = Ship.create(kind: "7_3", name: "ship3")
    @ships = [@s1_1, @s4_2, @s7_3]
    @tom = players :tom
    @bob = players :bob
    @poor = players :poor_chap
  end
  
  test 'associations check' do
    
    #ship have clusters and proper stat
    @ships.each do |ship|
      assert_not_nil ship.clusters
      assert ship.kind == ship.stat.kind
    end
    
    #ship association when bought, and price
    cash_before = @tom.cash
    @tom.buy([@s1_1, @s4_2])
    assert @s1_1.owner == @tom
    assert @s4_2.owner == @tom
    assert @tom.cash == cash_before - (@s1_1.stat.price + @s4_2.stat.price)
    
    #unable to buy when no enough money
    cash_before = @poor.cash
    @poor.buy(@s7_3)
    assert @s7_3.owner == nil
    assert @poor.cash == cash_before
    assert_not_nil @poor.errors
    
    #ship selling
    cash_before = @tom.cash
    @tom.sell(@s1_1)
    assert @s1_1.owner == nil
    assert @tom.cash == cash_before + @s1_1.stat.price * 0.6
  end
end
