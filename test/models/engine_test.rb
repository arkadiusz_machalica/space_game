require 'test_helper'

class EngineTest < ActiveSupport::TestCase
  def setup
    @s4_1 = Ship.create(kind: "4_1", name: "ship2")
    @bob = players :bob
    
    part_stat_for('engine', ['1_1', '4_1', '3_2'])
    @e1_1 = Engine.create(kind: "1_1") #size 1
    @e3_2 = Engine.create(kind: "3_2") #size 3
    @e4_1 = Engine.create(kind: "4_1") #size 4
    
    @bob.buy([@s4_1, @e1_1, @e3_2])
  end
  
  test 'engine behavior' do
    #no engines installed
    assert_equal 0, @s4_1.engines.count
    
    #install fitting engines
    @s4_1.install_parts([@e1_1, @e3_2])
    assert_equal 2, @s4_1.engines.count
    cluster1 = @s4_1.propulsion_clusters.first
    assert_equal @e1_1.stat.size + @e3_2.stat.size, cluster1.engine_slots
    
    #uninstalling engine
    @e1_1.reload
    @s4_1.uninstall_parts(@e1_1)
    cluster1.reload
    assert_equal @e3_2.stat.size, cluster1.engine_slots
    
    #installing engine that don't fit
    @s4_1.install_parts(@e4_1)
    cluster1.reload
    assert_equal @e3_2.stat.size, cluster1.engine_slots
    assert_not_nil @s4_1.errors
    
    #engine have proper HP value when created
    assert_equal @e4_1.stat.hp_base, @e4_1.hp
  end
end
