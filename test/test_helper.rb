ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  
  def part_stat_for(section, kinds)
    stats = YAML.load(File.read(
      File.expand_path("../../lib/assets/#{section}_types.yml", __FILE__)
      )).deep_symbolize_keys
    kinds.each do |kind|
    PartStat.create(
      section: section,
      kind: kind,
      stats: stats[section.to_sym][:"t_#{kind}"].to_s
      )
    end
  end

  # Add more helper methods to be used by all tests here...
end
