require 'test_helper'

class InspectionsControllerTest < ActionController::TestCase
  test "should get ship" do
    get :ship
    assert_response :success
  end

  test "should get cluster" do
    get :cluster
    assert_response :success
  end

  test "should get part" do
    get :part
    assert_response :success
  end

  test "should get worker" do
    get :worker
    assert_response :success
  end

end
