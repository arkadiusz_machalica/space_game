Rails.application.routes.draw do
  
  root 'statics#main'
  devise_for :players
  
  resources :ships
  get 'ships/new/:id' => 'ships#new_ship_offer', as: :new_ship_offer
  get 'ships/:id/:section/report' => 'ships#section_report', as: :section_report
  
  resources :workers
  post 'courses' => 'courses#create'
  get 'courses/price' => 'courses#price'
  
  delete 'parts/:klass/:id' => 'parts#destroy', as: :sell_part
  patch 'parts/:klass/:id' => 'parts#update', as: :uninstall_part
  post 'parts/:klass/:id/discharge' => 'parts#discharge', as: :part_discharge
  
  get 'inspections' => 'inspections#index', as: :inspections
  get 'inspection/clusters' => 'inspections#clusters', as: :clusters_inspection
  get 'inspection/clusters/:id' => 'inspections#cluster', as: :cluster_inspection
  
  get 'inspection/ships' => 'inspections#ships', as: :ships_inspection
  get 'inspection/ships/:id' => 'inspections#ship', as: :ship_inspection

  get 'inspection/parts' => 'inspections#parts', as: :parts_inspection
  get 'inspection/parts/:klass/:id' => 'inspections#part', as: :part_inspection

  get 'inspection/workers' => 'inspections#workers', as: :workers_inspection
  get 'inspection/workers/:id' => 'inspections#worker', as: :worker_inspection
  
  
  

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
