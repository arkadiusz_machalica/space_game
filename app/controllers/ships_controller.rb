class ShipsController < ApplicationController
  
  before_action :permit_signed_player
  before_action :get_player_ship, only: [:section_report]
  
  def index
    
  end

  def show
    # action avalible for other players
  end
  
  def section_report
    respond_to do |format|
      format.js {
        if Cluster::PART_ASSIGNMENT[params[:section].to_sym].nil?
            flash[:alert] = I18n.t('flash.error.ship.section')
            render :js => "window.location.href = '#{ship_path(@ship,format: 'html')}'"
        else
          
        end
      }
    end
  end

  def new
    @variant = params[:variant]
    if @variant.nil?
      @ship_stats = ShipStat.all[0...-1]
    else
      @ship_stats = ShipStat.where(variant: @variant)
    end
  end
  
  def new_ship_offer
    @ship_stat = ShipStat.find_by(id: params[:id])
    respond_to :js
  end
  
  def create
    if (@ship = Ship.new(
      kind: params[:kind],
      name: Ship.random_name,
      )) && @current_player.can_afford_ship?(params[:kind])
    
      @ship.save
      @current_player.buy(@ship)
      redirect_to @ship, flash: {notice: I18n.t('flash.successful.ship.creation')} and return
      
    else
      redirect_to new_ship_path, flash: {alert: I18n.t('flash.error.ship.creation')} and return
    end
  end

  def edit
    
  end
  
  def update
    
  end

  def delete
    
  end
  
  private
    
    def permit_signed_player
      if !player_signed_in?
        redirect_to new_player_session_path, flash: {alert: I18n.t('flash.error.player.not_logged_in')} and return
      end
    end
  
end
