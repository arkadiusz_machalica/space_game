class WorkersController < ApplicationController
  
  before_action :get_player_worker
  
  def update
    part = @worker.crewable
    part.discharge_workers(workers: @worker)
    if part.errors.empty?
      redirect_to root_path, flash: {notice: I18n.t("flash.success.worker.dismiss")}
    else
      redirect_to root_path, flash: {alert: I18n.t("flash.error.worker.dismiss")}
    end
  end
  
  def destroy
    @current_player.sell(@worker)
    if @current_player.errors.empty?
      redirect_to root_path, flash: {notice: I18n.t("flash.success.worker.destroy")}
    else
      redirect_to root_path, flash: {alert: I18n.t("flash.error.worker.destroy")}
    end
  end
  
  private
    
    def get_player_worker
      @worker = @current_player.workers.find_by(id: params[:id])
      if @worker.nil?
        redirect_to root_path, flash: {error: I18n.t("flash.error.worker.not_exist")}
      end
    end
  
end
