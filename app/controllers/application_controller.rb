class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :current_player, :get_current_player
  before_action :configure_permitted_parameters, if: :devise_controller?

  def get_current_player
    @current_player = current_player
  end
  
  def get_player_ship(options={})
    id = options[:id] || params[:id]
    @ship = @current_player.ships.find_by(id: id)
    if @ship.nil?
      redirect_to root_path, flash: {alert: I18n.t('flash.error.player.not_owner')} and return
    else
      true
    end
  end
  
  def get_player_worker(options={})
    id = options[:id] || params[:id]
    @worker = @current_player.workers.find_by(id: id)
    if @worker.nil?
      redirect_to root_path, flash: {alert: I18n.t('flash.error.player.not_owner')} and return
    else
      true
    end
  end
  
  def get_player_part(options={})
    klass = options[:klass] || params[:klass]
    id = options[:id] || params[:id]
    if !safe_klass?(klass)
      redirect_to root_path, flash: {alert: I18n.t('flash.error.part.wrong_klass')} and return
      return false
    end
    @part = @current_player.send(klass.downcase.pluralize).find_by(id: id)
    if @part.nil?
      redirect_to root_path, flash: {alert: I18n.t('flash.error.player.not_owner')} and return
    else
      true
    end
  end
  
  def safe_klass?(param=nil)
    param = param || params[:klass]
    Partable::KLASSES.include? param
  end
  
  def safe_section?(param=nil)
    param = param || params[:section]
    Cluster::PART_ASSIGNMENT.include? param
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
  end
  
end
