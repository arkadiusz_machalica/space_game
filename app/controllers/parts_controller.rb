class PartsController < ApplicationController
  
  before_action :get_player_sanitized_part
  
  def discharge
    if params[:all]
      workers = nil
    else
      worker_ids = params[:worker_ids].map {|k, v| v.to_i}
      workers = Worker.where(id: worker_ids)
    end
    @part.discharge_workers(workers: workers)
    respond_to do |format|
      format.js {
        
        flash[:notice] = I18n.t('flash.success.part.workers_discharge')
        render :js => "window.location.href = '#{
          ship_path(
            @part.ship,
            order: 'parts',
            part_id: @part.id,
            klass: @part.big_klass,
            format: 'html'
            )
        }'"
        
      }
    end
  end
  
  def update
    ship = @part.ship
    ship.uninstall_parts(@part)
    if ship.errors.empty?
      redirect_to root_path, flash: {notice: I18n.t("flash.success.part.uninstallation")} and return
    else
      redirect_to root_path, flash: {alert: I18n.t("flash.error.part.uninstallation")} and return
    end
  end
  
  def destroy
    ship = @part.ship
    @current_player.sell(@part)
    if @current_player.errors.empty? && ship.errors.empty?
      redirect_to root_path, flash: {notice: I18n.t("flash.success.part.sell")} and return
    else
      redirect_to root_path, flash: {alert: I18n.t("flash.error.part.sell")} and return
    end
  end
  
  private
    
    def get_player_sanitized_part
      if safe_klass?
        @part = @current_player.send(params[:klass].downcase.pluralize).find_by(id: params[:id])
        if @part.nil?
          redirect_to root_path, flash: {alert: I18n.t('flash.error.part.not_exist')}
        end
      else
        redirect_to root_path, flash: {alert: I18n.t('flash.error.part.wrong_klass')}
      end
    end
  
  
end
