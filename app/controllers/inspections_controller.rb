class InspectionsController < ApplicationController
  
  def index 
    @text = I18n.t('elements.inspection.index')
  end
  
  def ships
    @text = I18n.t('elements.inspection.ships')
    @ships = @current_player.ships
    if params[:s_with_tier] && params[:s_tiers]
      s_tiers = params[:s_tiers].values.map(&:to_i)
      @ships = @ships.by_tiers(s_tiers)
    end
    if params[:s_of_variant] && params[:s_variants]
      s_variants = params[:s_variants].values.map(&:to_i)
      @ships = @ships.by_variants(s_variants)
    end
    respond_to do |format|
      format.html {
        if @ships.empty?
          redirect_to inspections_path, flash: {alert: I18n.t('flash.error.ship.ships_not_found')}
        end
      }
      format.json { render json: {ships_found: @ships.count} }
    end
  end
  
  def ship
    get_player_ship
    @ship_stat = @ship.stat
  end
  
  def clusters
    get_player_ship(id: params[:c_ship_id])
    if params[:c_specific_section] && safe_section?(params[:c_section])
      cluster_expr = "#{params[:c_section]}_clusters"
      @clusters = @ship.send(cluster_expr)
    else
      @clusters = @ship.clusters
    end
  end
  
  def cluster
    get_player_ship(id: params[:c_ship_id])
    @cluster = @ship.clusters.find_by(id: params[:id])
    
    respond_to do |format|
      format.html
      format.js
    end
  end

  def parts
    if params[:p_selection] == 'all'
      part_params = {}
      @text = I18n.t('elements.inspection.parts.all')
    elsif params[:p_selection] == 'free'
      part_params = {ship_id: nil}
      @text = I18n.t('elements.inspection.parts.not_installed')
    elsif params[:p_selection] == 'shipped'
      get_player_ship(id: params[:p_ship_id])
      part_params = {ship_id: params[:p_ship_id]}
      @text = I18n.t('elements.inspection.parts.installed', name: @ship.name)
    end
    
    if params[:p_specific_klass] == 'true' && safe_klass?(params[:p_klass])
      part_expr = params[:p_klass].downcase.pluralize # returns engines for Engine
      @parts = @current_player.send(part_expr).where(part_params)
    else
      @parts = @current_player.parts(part_params)
    end
  rescue NoMethodError
    @parts = [[]]
  ensure
    respond_to do |format|
      format.html
      format.json { render json: {parts_found: @parts.flatten.count}}
    end
  end
  
  def part
    get_player_part
    @hp_index = @part.hp_index
    respond_to do |format|
      format.html
      format.js
    end
  end

  def workers
    @workers = @current_player.workers
    @text = I18n.t('elements.worker.all_employed')
    if params[:w_selection] == 'free'
      @workers = @workers.free
      @text = I18n.t('elements.worker.not_assigned')
    elsif params[:w_selection] == 'not_shipped'
      @workers = @workers.not_shipped
      @text = I18n.t('elements.worker.not_shipped')
    elsif params[:w_selection] == 'shipped' && get_player_ship(id: params[:w_ship_id])
      @workers = @ship.workers
      @text = I18n.t('elements.worker.shipped', name: @ship.name)
    end
    respond_to do |format|
      format.html
      format.json {render json: {workers_found: @workers.count}}
    end
  end
  
  def worker
    get_player_worker
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  private
    
end
