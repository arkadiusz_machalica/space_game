class CoursesController < ApplicationController
  
  
  def price
    @worker = Worker.find_by(id: params[:worker_id])
    points = params[:points].to_i
    relative = (params[:relative] == 'true' ? true : false )
    topic = params[:topic]
    price = @worker.training_price(topic, points, relative)
    affordable = @current_player.can_afford?(price)
    respond_to do |format|
      format.json { render json: {price: price, affordable: affordable}}
    end
  end
  
  def create
    @worker = Worker.find_by(id: params[:worker_id])
    if !@current_player.owner?(@worker)
      flash.now[:alert] = I18n.t('flash.error.player.not_owner')
      render 'inspections/worker' and return
    end
    topic = params[:topic]
    points = params[:course_points].to_i
    relative = (params[:relative] == 'true' ? true : false )
    price = @worker.training_price(topic, points, relative)
    if !@current_player.can_afford?(price)
      flash.now[:alert] = I18n.t('flash.error.course.too_expensive')
      render 'inspections/worker' and return
    end
    Player.transaction do
      @current_player.pay(price)
      @worker.train(topic, points, relative)
    end
    respond_to do |format|
      format.html
      format.js { 
        flash[:notice] = I18n.t('flash.success.worker.trained')
        redirect_to worker_inspection_path(@worker)
        # render :js => "window.location.href = '#{worker_inspection_path(@worker, format: 'html')}'"
      }
    end
  end
  
  
end
