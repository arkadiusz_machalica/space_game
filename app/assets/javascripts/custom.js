$(document).on("page:change", function() {
  
  /*
 * This function prevents default annoying behavior of # links
 * it's especially useful in the dev
 */
 
  $("a[href='#']").click(function(e) {
    e.preventDefault();
  });
  
});
