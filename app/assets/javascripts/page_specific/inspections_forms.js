// Ship form
$('#s_with_tier').change(function () {
  if (this.checked) {
    $('#s-tiers-checkboxes').show();
  } else {
    $('#s-tiers-checkboxes').hide();
    $('.s-tier-check').prop('checked', false);
  }
});

$('#s_of_variant').change(function () {
  if (this.checked) {
    $('#s-variants-checkboxes').show();
  } else {
    $('#s-variants-checkboxes').hide();
    $('.s-variant-check').prop('checked', false);
  }
});

$('.s-form-element').change(function() {
  var tiers = {};
  var variants = {};
  var path = $('#s_found').data('path');
  
  var withTier = $('#s_with_tier:checked').val();
  $('.s-tier-check:checked').each(function() {
    tiers[$(this).val()] = $(this).val();
  });
  console.log('with tier: ' + withTier);
  // console.log('tiers: ' + JSON.stringify(tiers));
  
  var ofVariant = $('#s_of_variant:checked').val();
  $('.s-variant-check:checked').each(function() {
    variants[$(this).val()] = $(this).val();
  });
  console.log('of variant: ' + ofVariant);
  // console.log('variants: ' + JSON.stringify(variants));
  if((withTier=='true'&& !$.isEmptyObject(tiers))||(ofVariant=='true'&& !$.isEmptyObject(variants))) {
    $.ajax(path, {
      success: function(response) {
        // console.log(response);
        $('#s_found').children('span').text(response.ships_found);
        $('#s_found').show();
        if( response.ships_found == 0 ) {
          $('#s-submit').hide();
        } else {
          $('#s-submit').show();
        }
      },
      data: {
        s_with_tier: withTier,
        s_tiers: tiers,
        s_of_variant: ofVariant,
        s_variants: variants
      },
      dataType: 'json'
    });
  } else {
    $('#s-submit').hide();
    $('#s_found').hide();
  }
});


// Part form
$('#p_selection_free').click(function () {
  $('#p_ship_id').hide();
});

$('#p_selection_shipped').click(function () {
  $('#p_ship_id').show();
});

$('#p_specific_klass').change(function () {
  if (this.checked) {
    $('#p_klass').show();
  } else {
    $('#p_klass').hide();
  }
});

$('.p-form-element').change(function() {
  var selection = $('#p-selections [name=p_selection]:checked').val();
  // console.log('selection: ' + selection);
  var shipId = $('#p_ship_id').val();
  // console.log('ship_id: ' + shipId);
  var specific = $('#p_specific_klass:checked').val();
  // console.log('specific: ' + specific);
  var klass = $('#p_klass').val();
  // console.log('klass: ' + klass);
  var path = $('#p_found').data('path');
  
  $.ajax(path, {
    success: function(response) {
      // console.log(response);
      $('#p_found').children('span').text(response.parts_found);
      $('#p_found').show();
      if( response.parts_found == 0 ) {
        $('#p-submit').hide();
      } else {
        $('#p-submit').show();
      }
    },
    data: {
      p_selection: selection,
      p_ship_id: shipId,
      p_specific_klass: specific,
      p_klass: klass
    },
    dataType: 'json'
  });
});


// Worker form
$("#w-selections").on('change', function() {
  if($('[name=w_selection]:checked').val() == 'shipped') {
    $('#w_ship_id').show();
  } else {
    $('#w_ship_id').hide();
  }
});

$('.w-form-element').change(function() {
  var selection = $('#w-selections [name=w_selection]:checked').val();
  // console.log('selection: ' + selection);
  var shipId = $('#w_ship_id').val();
  // console.log('ship_id: ' + shipId);
  var path = $('#w_found').data('path');
  $.ajax(path, {
    success: function(response) {
      // console.log(response);
      $('#w_found').children('span').text(response.workers_found);
      $('#w_found').show();
      if( response.workers_found == 0 ) {
        $('#w-submit').hide();
      } else {
        $('#w-submit').show();
      }
    },
    data: {
      w_selection: selection,
      w_ship_id: shipId
    },
    dataType: 'json'
  });
});
