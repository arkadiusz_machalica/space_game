$('#btn-training-form').click(function(e) {
  e.preventDefault();
  $('#training-form').toggle();
});

$('#topic').change(function() {
  if($(this).val() == '') {
    $('#hide-lvl1').hide();
  } else {
    $('#hide-lvl1').show();
  }
});

$('#train_by').click(function() {
  var points = $('#points-label');
  points.text($(this).data('translation'));
});

$('#train_to').click(function() {
  var points = $('#points-label');
  points.text($(this).data('translation'));
});

$('.form-element').change(function() {
  var points = $('#course_points').val();
  var topic = $('#topic').val();
  var relative = $('input[name=relative]:checked').val();
  var workerId = $('#worker_id').val();
  var path = $('#course-price').data('path');
  
  if((points > 0) && (points < 101) && (topic.length)) {
  
    $.ajax(path, {
      success: function(response) {
        $('#course-price > span').text('').text(response.price);
        if(response.price == 0 || !response.affordable) {
          $('#hide-lvl2').hide();
        } else {
          $('#hide-lvl2').show();
        }
        if(response.affordable) {
          $('#course-price-alert').hide();
        } else {
          $('#course-price-alert').show();
        }
      },
      data: {
        'topic': topic,
        'relative': relative,
        'points': points,
        'worker_id': workerId
      },
      dataType: 'json'
      
    });
    
  } else {
    
    $('#hide-lvl2').hide();
    
  }
  
});