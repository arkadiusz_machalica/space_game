class Cluster < ActiveRecord::Base
  
  SECTION_ASSIGNMENT = {
    'engine' => 'propulsion',
    'bay' => 'storage',
    'bridge' => 'command',
    'kineticgun' => 'arsenal',
    'emp' => 'arsneal',
    'hack' => 'arsenal',
    'plating' => 'armor',
    'barrier' => 'armor',
    'firewall' => 'armor'
  }
  PART_ASSIGNMENT = {
    propulsion: [:engine],
    storage: [:bay],
    command: [:bridge],
    arsenal: [:kineticgun, :emp, :hack],
    armor: [:plating, :barrier, :firewall]
  }
  belongs_to :ship
  has_many :engines #, :kineticguns, :platings, :bays, bridges
  
  validates :section,
    presence: true
  validates :ship_id,
    presence: true
  validate :proper_section?
  
  scope :for_ship_section, ->(sh_id, sec) { where(ship_id: sh_id, section: sec) }
  
  def name
    I18n.t("activerecord.attributes.ship.#{self.section}_cluster.one") +
    I18n.t("activerecord.attributes.ship.cluster_num", num: self.section_num)
  end
  
  def to_a
    [self]
  end
  
  def parts
    parts_collection = []
    
    # Uncomment when all section models will be created
    
    # SECTION_ASSIGNMENT.each_key do |part_type|
    #   parts_type = self.send(part_type.pluralize).to_a
    #   if !parts_type.empty?
    #     parts_collection << parts_type
    #   end
    # end
    
    # Temporary implementation. Remove when uncomment above!!
    parts_collection << self.engines.to_a if !self.engines.to_a.empty?
    
    return parts_collection.flatten
  end
  
  def workers
    workers_collection = []
    self.parts.each do |part|
      workers_collection << part.workers.to_a
    end
    return workers_collection.flatten
  end
  
  def stat
    eval(stats)
  end
  
  def free_slots_for(klass)
    klass_expr = "#{klass.downcase}_slots"
    begin
      self.stat[klass_expr.to_sym] - self.try(klass_expr)
    rescue
      return 0
    end
    
  end
  
  private
    def proper_section?
      %w(propulsion storage armor arsenal command).include? self.section
    end
end
