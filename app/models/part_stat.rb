class PartStat < ActiveRecord::Base
  
  # PartStat is general class which instances holds max 
  # possible values for part of given kind and section.
  # By looking to stat you can find out how are base stats for parts.
  include Statsable # instances contain stats
  has_many :engines #, :kineticguns, :platings, :captbridges
  
  # Meaningful methods (depending on section)
    # price
    # size
    # crew_min
    # crew_max
    # hp_base
    # def_base
    # dmg_base
    # power_base
    # speed_base
    # regen_base
    # parts_base
    
  
end
