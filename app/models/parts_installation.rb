class PartsInstallation
  
  def initialize(ship)
    @ship = ship
  end
  
  def create(options={})
    parts = options[:parts].to_a
    clusters = options[:clusters].to_a
    
    parts.each do |part|
      next if !same_owners?(part)
      klass = part.klass
      section = Cluster::SECTION_ASSIGNMENT[klass]
      
      if clusters.empty?
        #checks proper section clusters from ship
        clusters = @ship.clusters.where(section: section)
      else
        #checks proper section clusters from given
        clusters = clusters.select {|cluster| cluster.section == section}
      end
      
      # checks every cluster until part fits
      clusters.each do |cluster|
        if has_place?(cluster, part)
          install_part(cluster, part)
          break
        end
      end
      
      if part.cluster.nil?
        @ship.errors.add(:base, I18n.t('activerecord.errors.models.ship.no_fitting_cluster', part_name: part.name))
      end
    end
  end
  
  def destroy(options={})
    parts = options[:parts].to_a
    clusters = options[:clusters].to_a
    clusters = @ship.clusters if clusters.empty?
    
    if !parts.empty?
      parts.each do |part|
        next if !same_owners?(part)
        klass_expr = "#{part.klass}_slots"
        part.cluster.update(
          klass_expr.to_sym => part.cluster.send(klass_expr) - part.stat.size
          )
        part.update(cluster_id: nil, ship_id: nil)
        workers_ids = part.workers.pluck(:id)
        delegation = WorkersDelegation.new(part)
        delegation.update_workers(workers_ids, part.id, part.big_klass, nil)
      end
    else
      clusters.each do |cluster|
        next if !same_owners?(cluster)
        # Uncomment when all section models will be created
        
        # cluster.SECTION_ASSIGNMENT.each_key do |section|
        #   cluster.send(section.pluralize).update_all(cluster_id: nil, ship_id: nil)
        #   cluster.update(
        #     :"#{section}_slots" => 0
        #     )
        #   cluster.send(section.pluralize).each do |part|
        #     workers_ids = part.workers.pluck(:id)
        #     delegation = WorkersDelegation.new(part)
        #     delegation.update_workers(workers_ids, part.id, part.big_klass, nil)
        #   end
        # end
        
        # Temporary implementation. Remove when uncomment above!!
        cluster.engines.update_all(cluster_id: nil, ship_id: nil)
        cluster.update(
          :"engine_slots" => 0
          )
        cluster.engines.each do |part|
          workers_ids = part.workers.pluck(:id)
          delegation = WorkersDelegation.new(part)
          delegation.update_workers(workers_ids, part.id, part.big_klass, nil)
        end
        
      end
    end
    
  end
  
  def same_owners?(part)
    if @ship.owner == part.owner
      true
    else
      @ship.errors.add(:base, I18n.t('activerecord.errors.models.ship.owners_dont_match'))
      #part.errors.add(:base, I18n.t('activerecord.errors.models.part.owners_dont_match'))
      false
    end
  end
  
  def has_place?(cluster, part)
    klass_expr = "#{part.klass}_slots"
    all_slots = eval(cluster.stats)[klass_expr.to_sym]
    taken_slots = cluster.send(klass_expr)
    if (all_slots - taken_slots - part.stat.size) >= 0
      true
    else
      false
    end
  end
  
  def install_part(cluster, part)
    klass_expr = "#{part.klass}_slots"
    part.update(
      cluster_id: cluster.id,
      ship_id: cluster.ship_id
      )
    cluster.update(
      klass_expr.to_sym => cluster.send(klass_expr) + part.stat.size
      )
    delegation = WorkersDelegation.new(part)
    workers_ids = part.workers.pluck(:id)
    delegation.update_workers(workers_ids, part.id, part.big_klass, part.ship_id)
    
  end
  
  
end