class Engine < ActiveRecord::Base
  
  include Kindsable #instances have kinds (1_1, ..., 5_2, ..., 10_3)
  include Partable #instances are parts of the ship
  include Crewable #instances have crew
  
  before_create :assign_stats
  after_create :set_hp_to_index
  
  belongs_to :cluster
  belongs_to :ship
  belongs_to :owner, class_name: 'Player', foreign_key: :owner_id
  belongs_to :stat, class_name: 'PartStat', foreign_key: :stat_id
  
  has_many :workers, as: :crewable, dependent: :destroy
  
  validate :proper_kind?
  
end
