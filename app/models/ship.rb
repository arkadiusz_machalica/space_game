class Ship < ActiveRecord::Base
  
  include Kindsable #instances have kinds (1_1, ..., 5_2, ..., 10_3)
  
  attr_readonly [:kind, :stat_id]
  
  before_create :assign_stats, :set_tier_and_variant
  after_create :clusterize
  has_many :clusters, dependent: :destroy
  has_many :engines, dependent: :destroy
  has_many :workers, dependent: :destroy
  belongs_to :stat, class_name: 'ShipStat', foreign_key: 'stat_id'
  belongs_to :owner, class_name: 'Player', foreign_key: 'owner_id'
  
  validates :name,
    presence: true,
    length: { in: 5..45 },
    uniqueness: true
  validates :kind,
    presence: true
  validate :proper_kind?
  
  scope :by_tiers, ->(tiers) { where(tier: tiers) if tiers }
  scope :by_variants, ->(variants) { where(variant: variants) if variants }
  
  def propulsion_clusters
    Cluster.for_ship_section(self.id, 'propulsion')
  end
  
  def storage_clusters
    Cluster.for_ship_section(self.id, 'storage')
  end
  
  def armor_clusters
    Cluster.for_ship_section(self.id, 'armor')
  end
  
  def arsenal_clusters
    Cluster.for_ship_section(self.id, 'arsenal')
  end
  
  def command_clusters
    Cluster.for_ship_section(self.id, 'command')
  end
  
  def install_parts(parts, clusters=nil)
    installation = PartsInstallation.new(self)
    installation.create(parts: parts, clusters: clusters)
    
    # after installing parts it updates ship attributes
    #set_attr_values
  end
  
  def uninstall_parts(parts=nil, clusters=nil)
    installation = PartsInstallation.new(self)
    installation.destroy(parts: parts, clusters: clusters)
    
    
    # after installing parts it updates ship attributes
    #set_attr_values
  end
  
  def to_a
    [self]
  end
  
  def klass
    self.class.to_s.downcase
  end
  
  def big_klass
    self.class.to_s
  end
  
  def buy_by(owner)
    self.update_attribute(:owner_id, owner.id)
  end
  
  def sell
    self.destroy
  end
  
  def self.random_name
    Faker::Lorem.words(2).join(" ").capitalize
  end
  
  def parts
    parts_collection = []
    # final implementation not yet working
    
    # Cluster.SECTION_ASSIGNMENT.each_key do |part_type|
    #   parts_collection << self.send(part_type.pluralize).to_a
    # end
    
    # temporary implementation change when more part models will be added
    parts_collection << self.engines.to_a
    
    return parts_collection
  end
  
  private
  
    def assign_stats
      # assign ship to proper ShipStats
      self.stat_id = ShipStat.find_by(kind: self.kind).id
    end
    
    def set_tier_and_variant
      tier_and_variant = self.kind.split('_')
      self.tier = tier_and_variant.first
      self.variant = tier_and_variant.second
    end
    
    def clusterize
      # currently naive implementation
      stat.propulsion_clusters.times do |n|
        Cluster.create(
          section: 'propulsion',
          section_num: n+1,
          ship_id: self.id,
          stats: ({engine_slots: stat.engine_slots}).to_s)
      end
      
      stat.storage_clusters.times do |n|
        Cluster.create(
          section: 'storage',
          section_num: n+1,
          ship_id: self.id,
          stats: ({bay_slots: stat.bay_slots}).to_s)
      end
      
      stat.command_clusters.times do |n|
        Cluster.create(
          section: 'command',
          section_num: n+1,
          ship_id: self.id,
          stats: ({bridge_slots: stat.bridge_slots}).to_s)
      end
      
      stat.armor_clusters.times do |n|
        Cluster.create(
          section: 'armor',
          section_num: n+1,
          ship_id: self.id,
          stats: ({
            plating_slots: stat.plating_slots,
            barrier_slots: stat.barrier_slots,
            firewall_slots: stat.firewall_slots
            }).to_s
            )
      end
      
      stat.arsenal_clusters.times do |n|
        Cluster.create(
          section: 'arsenal',
          section_num: n+1,
          ship_id: self.id,
          stats: ({
            kineticgun_slots: stat.kineticgun_slots,
            emp_slots: stat.emp_slots,
            hack_slots: stat.hack_slots
            }).to_s
            )
      end
    end
end
