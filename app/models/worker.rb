class Worker < ActiveRecord::Base
  
  TOPICS = [:engine, :kineticgun, :plating, :bay]
  RANGES = {
    1 => (0..29),
    2 => (30..59),
    3 => (60..89),
    4 => (90..100)
  }
  
  belongs_to :employer, class_name: 'Player', foreign_key: 'employer_id'
  belongs_to :crewable, polymorphic: true
  belongs_to :ship
  
  validates :name,
    presence: true,
    uniqueness: true
  
  scope :best_in, ->(klass) { order("#{klass} DESC") }
  scope :worst_in, ->(klass) { order("#{klass} ASC") }
  scope :free, -> { where(crewable_type: nil) }
  scope :working, -> { where.not(crewable_type: nil) }
  scope :not_shipped, -> { working.where(ship_id: nil) }
  scope :working_on, ->(klass) { where(crewable_type: klass) if klass }
  
  def dismiss
    return nil if self.crewable_id.nil?
    self.update_attribute(:crewable_id, nil)
    self.update_attribute(:crewable_type, nil)
  end
  
  def hire_by(employer)
    self.update_attribute(:employer_id, employer.id)
  end
  
  def fire
    self.destroy
  end
  
  def skills
    skill_chart = TOPICS.map{ |topic|
      [topic, self.send(topic)]
    }
    skill_chart.sort_by!{|sub_arr| sub_arr[1]}.reverse!
  end
  
  def price
    price = 0
    factor = 1.0
    self.skills.take(3).each do |sub_arr|
      break if sub_arr[1] == 0 #skips if exp = 0
      price += self.formula(sub_arr[1], factor)
      factor *= 0.85
    end
    price |= 250 # if worker has no skills his price = 250
    return price
  end
  
  def training_price(topic, train_points, relative)
    skill_points = self.send(topic)
    train_points += skill_points if relative
    train_points = 100 if train_points > 100
    return 0 if train_points <= skill_points
    
    self.formula(train_points) - self.formula(skill_points)
  end
  
  def train(topic, train_points, relative)
    skill_points = self.send(topic)
    train_points += skill_points if relative
    train_points = 100 if train_points > 100
    return false if train_points <= skill_points
    self.update(topic.to_sym => train_points)
  end
  
  def self.instantize(options={})
    # shows num workers on given exp range, on given topic, (they are not created)
    # so workers that are not hired wont use space in db
    num = options[:num] || 1
    range = options[:range] || 1
    topic = options[:topic] || nil
    workers = []
    num.times do
      
      #if no topic given, it gets random one
      top = (topic.nil? ? TOPICS[rand(0..3)] : topic)
      
      #gets exp for given level
      exp_val = rand(RANGES[range])
      
      workers << self.new(
        name: (Faker::Name.name + '-' + Faker::Lorem.word).byteslice(0..26),
        top => exp_val
        )
      
    end
    return workers
  end
  
  def formula(lvl, factor=1.0)
    (250 * 1.06606 ** lvl * factor).to_i
  end
  
  def owner
    employer
  end
  
  def self.best
    # works properly only for groups of workers assigned on given part
    # eg. engine.workers.best
    klass = self.first.crewable.klass
    
    order("#{klass} DESC") unless klass.nil?
  end
  
  def self.worst
    # works properly only for groups of workers assigned on given part
    # eg. engine.workers.worst
    klass = self.first.crewable.klass
    
    order("#{klass} ASC") unless klass.nil?
  end
  
  def to_a
    [self]
  end
  
end
