module Partable
  
  ATTRIBUTES = {
    engine: [:power],
    kineticgun: [:dmg, :speed],
    plating: [:def]
  }
  
  KLASSES = %w(Engine Bay Bridge Kineticgun Emp Hack Plating Barrier Firewall)
  
  
  def name
    kind_array = self.kind.split('_')
    I18n.t("activerecord.attributes.#{self.class.to_s.downcase}.variants.t#{kind_array.last}") +
    I18n.t("activerecord.attributes.tier", tier: kind_array.first)
  end
  
  def install_to(cluster)
    installation = PartsInstallation.new(cluster.ship)
    installation.create(parts: self, clusters: cluster)
  end
  
  def uninstall
    return nil if self.cluster_id.nil?
    installation = PartsInstallation.new(self.ship)
    installation.destroy(parts: self)
  end
  
  def buy_by(owner)
    self.update_attribute(:owner_id, owner.id)
  end
  
  def sell
    self.destroy
  end
  
  def selling_price
    (stat.price * 0.969 ** (100 - (hp/stat.hp_base).to_i) * 0.6).to_i
  end
  
  def hp_index
    ((self.hp.to_f / self.stat.hp_base.to_f) * 100 ).floor
  end
  
  def klass
    self.class.to_s.downcase
  end
  
  def big_klass
    self.class.to_s
  end
  
  def to_a
    [self]
  end
  
  def set_attr_values
    #sets values of attributes according to efficiency
    
    #gets class of the part 
    klass = self.klass
    ATTRIBUTES[klass.to_sym].each do |attrib| # attrib for Engine => :power
      attrib_real = self.stat.send("#{attrib}_base") * total_efficiency
      self.update_attribute(attrib, attrib_real)
    end
    
    self.update_attribute(:parts, self.parts_base) if klass == 'plating'
  end
  
  def set_hp_to_index(val=1.0)
    # sets hp of given part to given percentage value
    return unless (0.0..1.0) === val #health need to be between 0% and 100%
    
    if self.class.to_s == 'Plating'
      # only for plating, hp depends on efficiency and parts
      hp_val = self.stat.def_base * self.total_efficiency * self.stat.parts_base * val
    else
      hp_val = self.stat.hp_base * val
    end
    self.update_attribute(:hp, hp_val)
    
    # after changing hp value it sets attr_values
    self.set_attr_values
    
    return self.hp
  end
  
  def occupied?
    self.people.count == self.crew_max
  end
  
  def total_efficiency
    #gets total part efficiency from crew skills and number
    
    hp_eff = hp_efficiency
    return 0.0 if hp_eff == 0.0
    num_eff = crew_num_efficiency
    return 0.0 if num_eff == 0.0
    exp_eff = exp_efficiency
    
    total_eff = hp_eff * num_eff * (1 + exp_eff)
    return total_eff.round(4)
  end
  
  def exp_efficiency
    #gets part efficiency according to crew skills
    
    #gets class of the part ie. engine, phys...
    klass = self.klass
    
    #gets array of crew exp for part class
    exp_chart = self.workers.pluck(klass)
    
    
    exp_eff = 0.0
    exp_chart.each do |ex|
      exp_eff += (ex.to_f * 0.0007 - 0.02).round(4)
    end
    
    #boundaries for effectiveness
    exp_eff = -0.2 if exp_eff < -0.2
    exp_eff = 0.5 if exp_eff > 0.5
    
    return exp_eff.round(4)
  end
  
  def crew_num_efficiency
    #gets part efficiency according to number of crew members
    
    crew_num = self.workers.count.to_f
    crew_min = self.stat.crew_min.to_f
    crew_max = self.stat.crew_max.to_f
    
    num_eff = if crew_num <= crew_min
      (crew_num / crew_min) * 0.8
    else
      0.8 + ((crew_num - crew_min) / (crew_max - crew_min)) * 0.2
    end
    
    return num_eff.round(4)
    
  end
  
  def hp_efficiency
    #gets part efficiency according to value of hp
    
    #physical armor efficiency does not depends on hp
    return 1.0 if self.klass == 'phys'
    
    hp_base = self.stat.hp_base.to_f
    hp_actual = self.hp.to_f
    hp_index = (hp_actual / hp_base).round(2)
    
    hp_eff = if hp_index >= 0.8
      1.0
    elsif hp_index >= 0.4
      (hp_index - 0.4) * 1.5 + 0.4
    else
      0.0
    end
    
    return hp_eff.round(4)
  end
  
  private
    
    def assign_stats
      # assign part to proper PartStats
      self.stat_id = PartStat.find_by(section: self.klass, kind: self.kind).id
    end
end