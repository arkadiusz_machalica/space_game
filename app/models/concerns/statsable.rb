module Statsable
  
  require 'yaml'
  
  def stat
    eval(self.stats)
  end
  
  def get_stats(*keys)
    #stats is string, when evaluated it becomes hash
    stats_hash = eval(self.stats)
    case keys.count
      when 1 then stats_hash[keys[0]]
      when 2 then stats_hash[keys[0]][keys[1]]
      when 3 then stats_hash[keys[0]][keys[1]][keys[2]]
    end
    rescue
      return nil
  end
  
  
  def price
    get_stats(:price)
  end
  
  def size
    get_stats(:size)
  end
  
  def crew_min
    get_stats(:crew_min)
  end
  
  def crew_max
    get_stats(:crew_max)
  end
  
  def hp_base
    get_stats(:hp_base)
  end
  
#--- begginning of ship specific part ---
  def propulsion_clusters
    get_stats(:propulsion,:clusters)
  end
  
  def engine_slots
    get_stats(:propulsion, :slots, :engine)
  end
  
  def arsenal_clusters
    get_stats(:arsenal,:clusters)
  end
  
  def kineticgun_slots
    get_stats(:arsenal,:slots,:kineticgun)
  end
  
  def emp_slots
    get_stats(:arsenal,:slots,:emp)
  end
  
  def hack_slots
    get_stats(:arsenal,:slots,:hack)
  end
  
  def armor_clusters
    get_stats(:armor,:clusters)
  end
  
  def plating_slots
    get_stats(:armor,:slots,:plating)
  end
  
  def barrier_slots
    get_stats(:armor,:slots,:barrier)
  end
  
  def firewall_slots
    get_stats(:armor,:slots,:firewall)
  end
  
  def storage_clusters
    get_stats(:storage,:clusters)
  end
  
  def bay_slots
    get_stats(:storage,:slots,:bay)
  end
  
  def command_clusters
    get_stats(:command,:clusters)
  end
  
  def bridge_slots
    get_stats(:command, :slots, :bridge)
  end
#--- end of ship specific part ---

  def power_base
    #for engines and cos
    get_stats(:power_base)
  end
  
  def dmg_base
    #for kinets and emps
    get_stats(:dmg_base)
  end
  
  def speed_base
    #for kinets and emps
    get_stats(:speed_base)
  end
  
  def regen_base
    #for barr
    get_stats(:regen_base)
  end
  
  def def_base
    #for barr phys
    get_stats(:def_base)
  end
  
  def parts_base
    #for phys
    get_stats(:parts_base)
  end

  
end