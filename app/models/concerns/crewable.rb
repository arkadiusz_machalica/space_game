module Crewable
  
  
  def delegate_workers(options={})
    workers = options[:workers]
    best = options[:best] || true
    delegation = WorkersDelegation.new(self)
    delegation.create(workers: workers, best: best)
  end
  
  def discharge_workers(options={})
    workers = options[:workers]
    num = options[:num]
    best = options[:best] || false
    delegation = WorkersDelegation.new(self)
    delegation.destroy(workers: workers, num: num, best: best)
  end
  
  
end