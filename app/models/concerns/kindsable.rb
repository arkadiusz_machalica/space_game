module Kindsable

  START_KINDS = {
    'ship' => ["11_4"],
    'engine' => ["10_4", "10_5", "10_6", "11_1"],
    'kinetic' => ["11_1","11_2","11_3"],
    'plating' => ["1_4", "1_5", "1_6", "10_4", "10_5", "10_6", "11_1","11_2","11_3"]
  }
  
  def proper_kind?
    unless build_kinds(START_KINDS[self.klass]).include? self.kind
      errors.add(:kind, I18n.t("activerecord.errors.models.#{self.klass}.attributes.kind.no_such_code"))
      return false
    end
  end
  
  def kind_name
    name = I18n.t("activerecord.attributes.#{self.klass}.variants.t#{self.variant}")
    name << I18n.t("activerecord.attributes.tier", tier: self.tier)
  end
  
  private
    
    def build_kinds(arr)
      (1..10).each do |m|
        (1..3).each { |n| arr << "#{m}_#{n}" }
      end
      return arr
    end
  
  
end