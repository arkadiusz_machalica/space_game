class Player < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  before_create :set_cash
  validates :name,
    presence: true,
    length: {in: 4..25},
    uniqueness: true
  
  has_many :ships, foreign_key: 'owner_id', dependent: :destroy
  has_many :clusters, through: :ships
  has_many :workers, foreign_key: 'employer_id', dependent: :destroy
  has_many :engines, foreign_key: 'owner_id', dependent: :destroy
  
  def buy(elements)
    elements.to_a.each do |element|
      Player.transaction do
        if element.owner.nil?
          pay_and_get(element)
        else
          self.errors.add(:base, I18n.t('activerecord.errors.models.player.element_not_free'))
          raise ActiveRecord::Rollback
        end
      end
    end
  end
  
  def sell(elements)
    elements.to_a.each do |element|
      Player.transaction do
        raise ActiveRecord::Rollback if !owner?(element)
        if element.is_a? Worker
          delegation = WorkersDelegation.new(element.crewable)
          delegation.destroy(workers: element)
        elsif element.is_a? Ship
          element.uninstall_parts
        else
          element.discharge_workers
          element.ship.uninstall_parts(element)
        end
        get_paid_and_sell(element)
      end
    end
  end
  
  def parts(options={})
    parts_collection = []
    # final implementation not yet working
    
    # Cluster.SECTION_ASSIGNMENT.each_key do |part_type|
    #   parts_collection << self.send(part_type.pluralize).where(options).to_a
    # end
    
    # temporary implementation change when more part models will be added
    parts_collection << self.engines.where(options).to_a
    
    return parts_collection
  end
  
  def pay(price)
    self.update(cash: cash - price)
  end
  
  def get_paid(price)
    self.update(cash: cash + price)
  end
  
  def can_afford_ship?(kind)
    can_afford?(ShipStat.find_by(kind: kind).price)
  end
  
  def can_afford?(price)
    return self.cash >= price
  end
  
  def owner?(element)
    if element.owner == self
      true
    else
      self.errors.add(:base, I18n.t('activerecord.errors.models.player.not_element_owner'))
      false
    end
  end
  
  private
    
    def pay_and_get(element)
      
      price = if element.is_a? Worker
        element.price
      else
        element.stat.price
      end
      
      if cash >= price
        
        self.pay(price)
        
        if element.is_a? Worker
          element.hire_by(self)
        else
          element.buy_by(self)
        end
        
      else
        self.errors.add(:base, I18n.t('activerecord.errors.models.player.not_enough_cash'))
        raise ActiveRecord::Rollback
      end
      
    end
    
    def get_paid_and_sell(element)
      
      if element.is_a? Worker
        price = (element.price * 0.55).to_i
        element.fire
      elsif element.is_a? Ship
        price = (element.stat.price * 0.6).to_i
        element.sell
      else
        price = element.selling_price
        element.sell
      end
      
      self.get_paid(price)
      
    end
    
    def set_cash
      self.cash = 20_000
    end
end
