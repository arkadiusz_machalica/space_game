class ShipStat < ActiveRecord::Base
  
  # ShipStat is general class which instances holds max possible values 
  # for ship of given class by looking to stat you can find out how many engines 
  # of which size (or other parts) can be mounted on given ship.
  include Statsable # instances contain stats
  before_create :set_tier_and_variant
  has_many :ships
  
  # Meaningful stat methods:
    # price
    # propulsion_clusters
      # engine_slots
    # arsenal_clusters
      # kineticgun_slots
      # emp_slots
      # hack_slots
    # armor_clusters
      # plating_slots
      # barrier_slots
      # firewall_slots
    # storage_clusters
      # bay_slots
    # command_clusters
      # bridge_slots
  
  def name
    I18n.t("activerecord.attributes.ship.variants.t#{self.variant}.one") +
    I18n.t("activerecord.attributes.tier", tier: self.tier)
  end
  
  private
    def set_tier_and_variant
      tier_and_variant = self.kind.split('_')
      self.tier = tier_and_variant.first
      self.variant = tier_and_variant.second
    end
  
end
