class WorkersDelegation
  
  attr_reader :part
  def initialize(part, options={})
    @part = part
    @klass = @part.klass
  end
  
  def create(options={})
    workers = options[:workers].to_a
    best = options[:best] || true
    
    # orders workers ascending by their skill in given part
    workers.sort_by!{ |worker| worker.send(@klass) }
    
    # best skilled workers will be asserted first if best==true
    workers.reverse! if best
    
    selected_ids = []
    confirmed_workers = 0
    
    # checks if there is place on the part, and part owner is also worker employer
    # if both are true it adds worker.id to id list
    workers.each do |worker|
      break if !has_place?(worker, confirmed_workers)
      next if !same_owners?(worker)
      
      selected_ids << worker.id
      confirmed_workers += 1
    end
    
    update_workers(selected_ids, @part.id, @part.big_klass, @part.ship_id)
    @part.set_attr_values
  end
  
  def destroy(options={})
    
    if options[:workers]
      # if given specific worker or collection of workers it dismisses them
      # this function has highest precedence
      selected_ids = options[:workers].to_a.map(&:id)
    elsif options[:num]
      # if given num it discharges num worst/best workers (depends on :best boolean)
      # worst workers are discharged by default
      ordering = (options[:best] == true ? 'DESC' : 'ASC')
      selected_ids = @part.workers.order("#{@klass} #{ordering}").limit(options[:num]).pluck(:id)
    else
      # all workers are discharged
      # this behaviour has lowest precedence
      selected_ids = @part.workers.pluck(:id)
    end
    
    update_workers(selected_ids, nil, nil, nil)
    @part.set_attr_values if !@part.nil?
  end
  
  def same_owners?(worker)
    if @part.owner == worker.employer
      true
    else
      #@part.errors.add(:base, I18n.t('activerecord.errors.models.part.players_dont_match'))
      worker.errors.add(:base, I18n.t('activerecord.errors.models.worker.owner_dont_match_employer'))
      false
    end
  end
  
  def has_place?(worker, confirmed_workers=0)
    if (@part.workers.count + confirmed_workers) < @part.stat.crew_max
      true
    else
      #@part.errors.add(:base, I18n.t('activerecord.errors.models.part.no_place'))
      worker.errors.add(:base, I18n.t('activerecord.errors.models.worker.no_place'))
      false
    end
  end
  
  def update_workers(workers_ids, id_value, type_value, ship_id)
    Worker.where(id: workers_ids).update_all(
      crewable_id: id_value,
      crewable_type: type_value,
      ship_id: ship_id
      )
  end
  
end